﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;
using System.Collections.Generic;


public class InputManager : MonoBehaviour {

    public class ControllerState
    {
        public enum CONTROLLERTYPE
        {
            KEYBOARD = 0,
            XBOXCONTROLLER,
			BOTH
        }

        public struct ControllerButtons
        {
            public ButtonState A { get; set; }
            public ButtonState B { get; set; }
            public ButtonState X { get; set; }
            public ButtonState Y { get; set; }
            public ButtonState Back { get; set; }
            public ButtonState Guide { get; set; }
            public ButtonState LeftShoulder { get; set; }
            public ButtonState LeftStick { get; set; }
            public ButtonState RightShoulder { get; set; }
            public ButtonState RightStick { get; set; }
            public ButtonState Start { get; set; }
        }
		public struct ControllerDpad
		{
			public ButtonState UP { get; set; }
			public ButtonState RIGHT { get; set; }
			public ButtonState DOWN { get; set; }
			public ButtonState LEFT { get; set; }
		}


        public CONTROLLERTYPE controllerType = CONTROLLERTYPE.KEYBOARD;
        public PlayerIndex playerIndex = PlayerIndex.One;

		// UP, RIGHT, DOWN, LEFT
		public Vector2 leftStick = Vector2.zero;
		public Vector2 rightStick = Vector2.zero;

		public ControllerDpad dPadState = new ControllerDpad();

		public ControllerButtons buttonStates = new ControllerButtons();

        public float triggerLeft = 0f;
        public float triggerRight = 0f;


        public void UpdateState()
        {
            switch (controllerType)
            {
                case CONTROLLERTYPE.KEYBOARD:

					buttonStates.A = Input.GetButton("A")? ButtonState.Pressed : ButtonState.Released;;
					buttonStates.B = Input.GetButton("B")? ButtonState.Pressed : ButtonState.Released;;
					buttonStates.X = Input.GetButton("X")? ButtonState.Pressed : ButtonState.Released;;
					buttonStates.Y = Input.GetButton("Y") ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.Back = Input.GetButton("Back") ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.Guide = Input.GetButton("Guide") ? ButtonState.Pressed : ButtonState.Released;

					buttonStates.LeftShoulder = Input.GetButton("Left Shoulder") ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.LeftStick = Input.GetButton("Left Stick") ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.RightShoulder = Input.GetButton("Right Shoulder") ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.RightStick = Input.GetButton("Right Stick") ? ButtonState.Pressed : ButtonState.Released;

					buttonStates.Start = Input.GetButton("Start")? ButtonState.Pressed : ButtonState.Released;

					leftStick.x = Input.GetAxis("Horizontal");
					leftStick.y = Input.GetAxis("Vertical");
					rightStick.x = Input.GetAxis("Mouse X");
					rightStick.y = Input.GetAxis("Mouse Y");

					dPadState.UP = Input.GetButton("Numpad Up") ? ButtonState.Pressed : ButtonState.Released;
					dPadState.RIGHT = Input.GetButton("Numpad Right") ? ButtonState.Pressed : ButtonState.Released;
					dPadState.DOWN = Input.GetButton("Numpad Down") ? ButtonState.Pressed : ButtonState.Released;
					dPadState.LEFT = Input.GetButton("Numpad Left") ? ButtonState.Pressed : ButtonState.Released;

					triggerLeft = Input.GetButton("Left Trigger") ? 1f : 0f;
					triggerRight = Input.GetButton("Right Trigger") ? 1f : 0f;

					break;
                case CONTROLLERTYPE.XBOXCONTROLLER:
                    GamePadState gamePadState =  GamePad.GetState(playerIndex);

                    XInputDotNetPure.GamePadButtons gamePadButtons = gamePadState.Buttons;
                    buttonStates.A = gamePadButtons.A;
                    buttonStates.B = gamePadButtons.B;
                    buttonStates.X = gamePadButtons.X;
                    buttonStates.Y = gamePadButtons.Y;
                    buttonStates.Back = gamePadButtons.Back;
                    buttonStates.Guide = gamePadButtons.Guide;
                    buttonStates.LeftShoulder = gamePadButtons.LeftShoulder;
                    buttonStates.LeftStick = gamePadButtons.LeftStick;
                    buttonStates.RightShoulder = gamePadButtons.RightShoulder;
                    buttonStates.RightStick = gamePadButtons.RightStick;
                    buttonStates.Start = gamePadButtons.Start;

					leftStick.x = gamePadState.ThumbSticks.Left.X;
					leftStick.y = gamePadState.ThumbSticks.Left.Y;
					rightStick.x = gamePadState.ThumbSticks.Right.X;
					rightStick.y = gamePadState.ThumbSticks.Right.Y;

					dPadState.UP = gamePadState.DPad.Up;
					dPadState.RIGHT = gamePadState.DPad.Right;
					dPadState.DOWN = gamePadState.DPad.Down;
					dPadState.LEFT = gamePadState.DPad.Left;

					triggerLeft = gamePadState.Triggers.Left;
					triggerRight = gamePadState.Triggers.Right;

					break;
				case CONTROLLERTYPE.BOTH:
					GamePadState padState = GamePad.GetState(playerIndex);

					XInputDotNetPure.GamePadButtons padButtons = padState.Buttons;

					buttonStates.A = Input.GetButton("A") || padButtons.A == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.B = Input.GetButton("B") || padButtons.B == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.X = Input.GetButton("X") || padButtons.X == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.Y = Input.GetButton("Y") || padButtons.Y == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.Back = Input.GetButton("Back") || padButtons.Back == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.Guide = Input.GetButton("Guide") || padButtons.Guide == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;

					buttonStates.LeftShoulder = Input.GetButton("Left Shoulder") || padButtons.LeftShoulder == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.LeftStick = Input.GetButton("Left Stick") || padButtons.LeftStick == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.RightShoulder = Input.GetButton("Right Shoulder") || padButtons.RightShoulder == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					buttonStates.RightStick = Input.GetButton("Right Stick") || padButtons.RightStick == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;

					buttonStates.Start = Input.GetButton("Start") || padButtons.Start == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;

					leftStick.x = Mathf.Clamp(Input.GetAxis("Horizontal") + padState.ThumbSticks.Left.X, -1f, 1f);
					leftStick.y = Mathf.Clamp(Input.GetAxis("Vertical") + padState.ThumbSticks.Left.Y, -1f, 1f);
					rightStick.x = Mathf.Clamp(Input.GetAxis("Mouse X") + padState.ThumbSticks.Right.X, -1f, 1f);
					rightStick.y = Mathf.Clamp(Input.GetAxis("Mouse Y") + padState.ThumbSticks.Right.Y, -1f, 1f);

					dPadState.UP = Input.GetButton("Numpad Up") || padState.DPad.Up == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					dPadState.RIGHT = Input.GetButton("Numpad Right") || padState.DPad.Right == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					dPadState.DOWN = Input.GetButton("Numpad Down") || padState.DPad.Down == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;
					dPadState.LEFT = Input.GetButton("Numpad Left") || padState.DPad.Left == ButtonState.Pressed ? ButtonState.Pressed : ButtonState.Released;

					triggerLeft = Input.GetButton("Left Trigger") ? 1f : padState.Triggers.Left;
					triggerRight = Input.GetButton("Right Trigger") ? 1f : padState.Triggers.Right;

					break;

            }
        }

        public ControllerState(CONTROLLERTYPE type, PlayerIndex index)
        {
			controllerType = type;
			playerIndex = index;
        }
    }


	public List<bool> gamePadsConnected = new List<bool>(4);
	public List<ControllerState> controllerStates = new List<ControllerState>();

	private static InputManager instance;

	public static InputManager Instance
	{
		get
		{
			if (instance == null)
			{
				GameObject inputGameObject = new GameObject("InputManager");
				inputGameObject.AddComponent<InputManager>();
				instance = inputGameObject.GetComponent<InputManager>();
            }
			return instance;
		}
	}

	public bool drawDebug = false;

	void Awake()
	{
		controllerStates.Add(new ControllerState(ControllerState.CONTROLLERTYPE.BOTH, PlayerIndex.One));
		controllerStates.Add(new ControllerState(ControllerState.CONTROLLERTYPE.XBOXCONTROLLER, PlayerIndex.Two));
		controllerStates.Add(new ControllerState(ControllerState.CONTROLLERTYPE.XBOXCONTROLLER, PlayerIndex.Three));
		controllerStates.Add(new ControllerState(ControllerState.CONTROLLERTYPE.XBOXCONTROLLER, PlayerIndex.Four));
	}

	void Start () {
		instance = this;

		gamePadsConnected.Add(false);
		gamePadsConnected.Add(false);
		gamePadsConnected.Add(false);
		gamePadsConnected.Add(false);
	}
	
	void Update () {

		ControllerCheck();

		foreach (ControllerState state in controllerStates)
		{
			state.UpdateState();
		}
	}

	public void VibrateController(int playerIndex, float leftMotor, float rightMotor, float seconds)
	{
		switch (playerIndex)
		{
			case 0:
				GamePad.SetVibration(PlayerIndex.One, leftMotor, rightMotor);
				StartCoroutine(ResetVibration(PlayerIndex.One, seconds));
				break;
			case 1:
				GamePad.SetVibration(PlayerIndex.Two, leftMotor, rightMotor);
				StartCoroutine(ResetVibration(PlayerIndex.Two, seconds));
				break;
			case 2:
				GamePad.SetVibration(PlayerIndex.Three, leftMotor, rightMotor);
				StartCoroutine(ResetVibration(PlayerIndex.Three, seconds));
				break;
			case 3:
				GamePad.SetVibration(PlayerIndex.Four, leftMotor, rightMotor);
				StartCoroutine(ResetVibration(PlayerIndex.Four, seconds));
				break;
		}
	}

	IEnumerator ResetVibration(PlayerIndex index, float seconds)
	{
		yield return new WaitForSeconds(seconds);
		GamePad.SetVibration(index, 0f, 0f);
	}

	void ControllerCheck()
	{
		/*
		// Check for new connected controllers
		bool gamePadConnected = GamePad.GetState(PlayerIndex.One).IsConnected;
		if (gamePadConnected != gamePadsConnected[0])
		{
			ControllerChanged(PlayerIndex.One, gamePadConnected);
			gamePadsConnected[0] = !gamePadsConnected[0];
		}
		gamePadConnected = GamePad.GetState(PlayerIndex.Two).IsConnected;
		if (gamePadConnected != gamePadsConnected[1])
		{
			ControllerChanged(PlayerIndex.Two, gamePadConnected);
			gamePadsConnected[1] = !gamePadsConnected[1];
		}
		gamePadConnected = GamePad.GetState(PlayerIndex.Three).IsConnected;
		if (gamePadConnected != gamePadsConnected[2])
		{
			ControllerChanged(PlayerIndex.Three, gamePadConnected);
			gamePadsConnected[2] = !gamePadsConnected[2];
		}
		gamePadConnected = GamePad.GetState(PlayerIndex.Four).IsConnected;
		if (gamePadConnected != gamePadsConnected[3])
		{
			ControllerChanged(PlayerIndex.Four, gamePadConnected);
			gamePadsConnected[3] = !gamePadsConnected[3];
		}
		*/
	}

	void ControllerChanged(PlayerIndex index, bool connected)
	{
		Debug.Log("Controller " + index + (connected? " Connected" : " Disconnected"));
	}

	public void ChangeControllerType(int playerIndex, ControllerState.CONTROLLERTYPE newType)
	{
		//controllerStates[(int)playerIndex].controllerType = newType;
	}

	public ControllerState getControllerState(int index, ControllerState.CONTROLLERTYPE type)
	{
		//ChangeControllerType(index, type);

		return controllerStates[index];
	}

	void OnGUI()
	{
		if (drawDebug)
		{
			int yBegin = 0;

			foreach (ControllerState state in controllerStates)
			{
				string text = "Use left stick to turn the cube, hold A to change color\n";
				text += string.Format("\tTriggers {0} {1}\n", state.triggerLeft, state.triggerRight);
				text += string.Format("\tD-Pad {0} {1} {2} {3}\n", state.dPadState.UP, state.dPadState.RIGHT, state.dPadState.DOWN, state.dPadState.LEFT);
				text += string.Format("\tButtons Start {0} Back {1} Guide {2}\n", state.buttonStates.Start, state.buttonStates.Back, state.buttonStates.Guide);
				text += string.Format("\tButtons LeftStick {0} RightStick {1} LeftShoulder {2} RightShoulder {3}\n", state.buttonStates.LeftStick, state.buttonStates.RightStick, state.buttonStates.LeftShoulder, state.buttonStates.RightShoulder);
				text += string.Format("\tButtons A {0} B {1} X {2} Y {3}\n", state.buttonStates.A, state.buttonStates.B, state.buttonStates.X, state.buttonStates.Y);
				text += string.Format("\tSticks Left {0} {1} Right {2} {3}\n", state.leftStick.x, state.leftStick.y, state.rightStick.x, state.rightStick.y);
				GUI.Label(new Rect(0, yBegin, Screen.width, Screen.height), text);

				yBegin += 200;
			}

			string controllersConnected = "";

			foreach (bool gamePadConnected in gamePadsConnected)
			{
				controllersConnected += gamePadConnected + "\n";
			}

			GUI.Label(new Rect(Screen.width - 200f, 0f, Screen.width, Screen.height), controllersConnected);
		}
	}

}
