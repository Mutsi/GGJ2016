﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XInputDotNetPure;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour {

	public enum WIZARDS
	{
		BLUE,
		GREEN,
		YELLOW,
		RED
	}

	public RectTransform player1Selector;
	public RectTransform player2Selector;

	public Image player1Wizard;
	public Image player2Wizard;

	

	public RectTransform blueWizard;
	public RectTransform greenWizard;
	public RectTransform yellowWizard;
	public RectTransform redWizard;

	public Vector2 selectorStartPosition = new Vector2(12.9f, -50.3f);

	public int player1Selection = 1;
	public int player2Selection = 1;
	public int player1InputSelection = 0;
	public int player2InputSelection = 0;


	public List<Sprite> sourceFullWizardImages = new List<Sprite>();

	float lastSwitchTime = 0f;

	float aHoldTime1 = 0f;
	float aHoldTime2 = 0f;

	void Start () {
		lastSwitchTime = Time.time;
    }

	void Update () {

		XInputDotNetPure.GamePadState stateController1 = GamePad.GetState(PlayerIndex.One);
		XInputDotNetPure.GamePadState stateController2 = GamePad.GetState(PlayerIndex.Two);

		if (stateController1.Buttons.A == ButtonState.Pressed)
		{
			SetWizard(1, player1Selection);
			aHoldTime1 += Time.deltaTime;
        }
		else if(stateController1.Buttons.A == ButtonState.Released)
		{
			aHoldTime1 = 0f;
		}
		if (stateController2.Buttons.A == ButtonState.Pressed)
		{
			SetWizard(2, player2Selection);
			aHoldTime2 += Time.deltaTime;
		}
		else if (stateController2.Buttons.A == ButtonState.Released)
		{
			aHoldTime2 = 0f;
		}

		if(aHoldTime2 > 1f || aHoldTime1 > 1f)
		{
			StartGame();
		}

		if (stateController1.ThumbSticks.Left.X > 0f || stateController1.DPad.Right == ButtonState.Pressed)
		{
			SetWizard(1, player1Selection + 1);
		}
		if (stateController1.ThumbSticks.Left.X < 0f || stateController1.DPad.Left == ButtonState.Pressed)
		{
			SetWizard(1, player1Selection - 1);
		}

		if (stateController2.ThumbSticks.Left.X > 0f || stateController2.DPad.Right == ButtonState.Pressed)
		{
			SetWizard(2, player2Selection + 1);
		}
		if (stateController2.ThumbSticks.Left.X < 0f || stateController2.DPad.Left == ButtonState.Pressed)
		{
			SetWizard(2, player2Selection - 1);
		}


		player1Selection = Mathf.Clamp(player1Selection, 0, 3);
		player2Selection = Mathf.Clamp(player2Selection, 0, 3);
		player1Selector.anchoredPosition = Vector2.Lerp(player1Selector.anchoredPosition, selectorStartPosition + Vector2.right * (player1Selection * 25f), 0.5f);
		player2Selector.anchoredPosition = Vector2.Lerp(player2Selector.anchoredPosition, selectorStartPosition + Vector2.right * (player2Selection * 25f), 0.5f);


	}

	public void SetWizard(int player, int wizardIndex)
	{
		if (wizardIndex >= sourceFullWizardImages.Count || wizardIndex < 0)
			return;

		if(Time.time - lastSwitchTime > 0.2f)
		{
			if (player == 1)
			{
				player1Selection = wizardIndex;
				player1Wizard.sprite = sourceFullWizardImages[wizardIndex];
			}
			else if (player == 2)
			{
				player2Selection = wizardIndex;
				player2Wizard.sprite = sourceFullWizardImages[wizardIndex];
			}
			lastSwitchTime = Time.time;
		}
    }



	public void StartGame()
	{
		PlayerPrefs.SetInt("wizardIndexPlayer1", player1Selection);
		PlayerPrefs.SetInt("wizardIndexPlayer2", player2Selection);

		SceneManager.LoadScene(1);
	}

	public void ExitGame()
	{

	}
}
