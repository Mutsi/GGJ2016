﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;
using System.Collections.Generic;
using UnityEngine.UI;

public class RitualBehaviour : MonoBehaviour
{
	int playerIndex;
	InputManager.ControllerState.CONTROLLERTYPE controllerType;

	bool castOnSelf = false;
	public string comboList = "";

	bool comboStarted = false;

	PlayerBehaviour playerBehaviour;
	RitualBehaviour enemyRitualBehaviour;

	XInputDotNetPure.ButtonState lastXstate = XInputDotNetPure.ButtonState.Released;
	XInputDotNetPure.ButtonState lastYstate = XInputDotNetPure.ButtonState.Released;
	XInputDotNetPure.ButtonState lastBstate = XInputDotNetPure.ButtonState.Released;

	public bool ColdFeetActive
	{
		get
		{
			return coldFeetTimer > 0.0f;
		}
	}

	public bool BreezeActive
	{
		get
		{
			return smallBreezeTimer > 0.0f;
		}
	}

	public bool ShieldActive
	{
		get
		{
			return shieldTimer > 0.0f;
		}
	}

	float smallBreezeTimer = -1f;
	float confusionTimer = -1f;
	float coldFeetTimer = -1f;
	float burstTimer = -1f;
	float doubleJumpTimer = -1f;
	float shieldTimer = -1f;

	float startSpeedMultiplier;

	public GameObject buttonImagePrefab;
	public List<Sprite> buttonGUISourceSprites = new List<Sprite>();

	public RectTransform comboButtonParent;
	public Image confusionClockImage;

	void Start()
	{
		playerBehaviour = GetComponent<PlayerBehaviour>();
		playerIndex = playerBehaviour.playerIndex;
		controllerType = playerBehaviour.controllerType;

		if (playerBehaviour.otherPlayer != null)
			enemyRitualBehaviour = playerBehaviour.otherPlayer.GetComponent<RitualBehaviour>();
		startSpeedMultiplier = playerBehaviour.SpeedMult;
	}

	void Update()
	{
		InputManager.ControllerState controllerState = InputManager.Instance.getControllerState(playerIndex, controllerType);

		playerBehaviour.RitualAllocPenalty = Mathf.Max(0.175f, 1.0f - (0.175f * comboList.Length));

		if (comboStarted)
		{
			Debug.Log("Update: comboStarted");

			string comboToAdd = "";

			Sprite buttonToDisplay = null;

			if (lastXstate == XInputDotNetPure.ButtonState.Released && controllerState.buttonStates.X == XInputDotNetPure.ButtonState.Pressed)
			{
				comboToAdd = "X";
				buttonToDisplay = buttonGUISourceSprites[2];
			}
			if (lastYstate == XInputDotNetPure.ButtonState.Released && controllerState.buttonStates.Y == XInputDotNetPure.ButtonState.Pressed)
			{
				comboToAdd = "Y";
				buttonToDisplay = buttonGUISourceSprites[3];
			}
			if (lastBstate == XInputDotNetPure.ButtonState.Released && controllerState.buttonStates.B == XInputDotNetPure.ButtonState.Pressed)
			{
				comboToAdd = "B";
				buttonToDisplay = buttonGUISourceSprites[1];
			}

			if (comboToAdd != "")
			{
				comboList += comboToAdd;
			}

			if (buttonToDisplay != null && comboList.Length <= 4)
			{
				GameObject newComboButton = Instantiate(buttonImagePrefab) as GameObject;
				newComboButton.transform.SetParent(comboButtonParent);

				Image newImage = newComboButton.GetComponent<Image>();
				newImage.sprite = buttonToDisplay;


				Animation buttonAnimation = newComboButton.GetComponent<Animation>();
				buttonAnimation.Rewind();
				buttonAnimation.Play();
			}

			// EndCombo
			if ((castOnSelf && controllerState.triggerRight == 0f) || (!castOnSelf && controllerState.triggerLeft == 0f))
			{
				Debug.Log("Update: EndCombo");
				EndCombo();

				playerBehaviour.PlaySound("cast");

				Image[] comboImageList = comboButtonParent.GetComponentsInChildren<Image>();

				for (int i = 0; i < comboImageList.Length; i++)
				{
					Destroy(comboImageList[i].gameObject);
				}

				InputManager.Instance.VibrateController(playerIndex, castOnSelf ? 0f : 1f, castOnSelf ? 1f : 0f, Mathf.Clamp(comboList.Length / 10f, 0f, 0.5f));
			}
		}

		if (controllerState.triggerLeft > 0.0f && !comboStarted)
		{
			StartCombo(false);
		}
		if (controllerState.triggerRight > 0.0f && !comboStarted)
		{
			StartCombo(true);
		}

		// Reverse effects of rituals on timer == 0f
		// Set timer to -1 to indicate it is inactive
		if (smallBreezeTimer == 0f)
		{
			smallBreezeTimer = -1f;
		}
		if (confusionTimer == 0f)
		{
			this.playerBehaviour.reverseControls = 1f;
			confusionTimer = -1f;
		}
		if (coldFeetTimer == 0f)
		{
			this.GetComponent<Rigidbody>().isKinematic = false;
			coldFeetTimer = -1f;
		}
		if (burstTimer == 0f)
		{
			this.playerBehaviour.SpeedMult = startSpeedMultiplier;
			burstTimer = -1f;
		}
		if (doubleJumpTimer == 0f)
		{
			doubleJumpTimer = -1f;
		}
		if (shieldTimer == 0f)
		{
			shieldTimer = -1f;
		}

		if (smallBreezeTimer != -1f)
			smallBreezeTimer = Mathf.Clamp(smallBreezeTimer - Time.deltaTime, 0f, 10f);
		if (confusionTimer != -1f)
		{
			confusionTimer = Mathf.Clamp(confusionTimer - Time.deltaTime, 0f, 10f);
			confusionClockImage.fillAmount = confusionTimer / 2f;
		}
		if (coldFeetTimer != -1f)
			coldFeetTimer = Mathf.Clamp(coldFeetTimer - Time.deltaTime, 0f, 10f);
		if (burstTimer != -1f)
			burstTimer = Mathf.Clamp(burstTimer - Time.deltaTime, 0f, 10f);
		if (doubleJumpTimer != -1f)
			doubleJumpTimer = Mathf.Clamp(doubleJumpTimer - Time.deltaTime, 0f, 10f);
		if (shieldTimer != -1f)
			shieldTimer = Mathf.Clamp(shieldTimer - Time.deltaTime, 0f, 10f);

		lastXstate = controllerState.buttonStates.X;
		lastYstate = controllerState.buttonStates.Y;
		lastBstate = controllerState.buttonStates.B;
	}

	void StartCombo(bool onSelf)
	{
		comboList = "";

		castOnSelf = onSelf;

		comboStarted = true;
	}

	void EndCombo()
	{
		comboStarted = false;

		if (comboList == "")
		{
			return;
		}

		RitualBehaviour target = this;
		if (!castOnSelf)
			target = enemyRitualBehaviour;

		target.ExecuteRitual(comboList, this);

		comboList = "";
	}

	public void ExecuteRitual(string ritual, RitualBehaviour sender)
	{
		if (shieldTimer > 0f && sender != this)
			return;


		bool ritualSucces = true;
		switch (ritual)
		{
			// Small Breeze - knock back enemy
			case "XXB":
				if (sender.playerBehaviour.Mana - ritual.Length < 0f)
				{
					ritualSucces = false;
					break;
				}

				this.smallBreezeTimer = Mathf.Clamp(this.smallBreezeTimer, 0f, 10f);
				this.smallBreezeTimer += 2f;
				this.GetComponent<Rigidbody>().AddForce(new Vector3(-800f, 0f, 0f), ForceMode.Force);
				break;
			// Confusion - revert controls
			case "XYBY":
				if (sender.playerBehaviour.Mana - ritual.Length < 0f)
				{
					ritualSucces = false;
					break;
				}

				this.playerBehaviour.reverseControls = -1f;
				// Make sure timer isn't -1
				confusionTimer = Mathf.Clamp(confusionTimer, 0f, 10f);
				confusionTimer += 2f;
				break;
			// Cold Feet - freeze opponent
			case "XBB":
				if (sender.playerBehaviour.Mana - ritual.Length < 0f)
				{
					ritualSucces = false;
					break;
				}

				this.GetComponent<Rigidbody>().isKinematic = true;
				coldFeetTimer = Mathf.Clamp(coldFeetTimer, 0f, 10f);
				coldFeetTimer += 1f;
				break;
			// Burst - increase move speed
			case "XXY":
				if (sender.playerBehaviour.Mana - ritual.Length < 0f)
				{
					ritualSucces = false;
					break;
				}

				this.playerBehaviour.SpeedMult = 20f;
				burstTimer = Mathf.Clamp(burstTimer, 0f, 10f);
				burstTimer += 2f;
				break;
			// SuperJump
			case "BY":
				if (sender.playerBehaviour.Mana - ritual.Length < 0f)
				{
					ritualSucces = false;
					break;
				}

				this.GetComponent<Rigidbody>().AddForce(playerBehaviour.JumpForce * 1.5f * Vector3.up);

				doubleJumpTimer = Mathf.Clamp(doubleJumpTimer, 0f, 10f);
				doubleJumpTimer += 2f;
				break;
			// Shield - counter enemy ritual
			case "YB":
				if (sender.playerBehaviour.Mana - ritual.Length < 0.2f)
				{
					ritualSucces = false;
					break;
				}

				shieldTimer = Mathf.Clamp(shieldTimer, 0f, 10f);
				shieldTimer += 2f;
				break;
			default:
				ritualSucces = false;
				break;
		}

		if (ritualSucces)
		{
			sender.playerBehaviour.Mana -= ritual.Length;

			if (sender == this)
			{
				playerBehaviour.PlaySound("Self");
			}
			else
			{
				sender.playerBehaviour.PlaySound("Hit");
			}
		}

	}
}
