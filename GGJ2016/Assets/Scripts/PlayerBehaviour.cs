﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour
{

	public List<AudioClip> ClipList = new List<AudioClip>();

	public float MaxSpeed = 5.0f;
	public float SpeedMult = 1.0f;
	public float RitualAllocPenalty = 1f;
	public float JumpForce = 300.0f;
	public float HorizontalFriction = 0.7f;
	public int MaxJumps = 1;
	public float reverseControls = 1f;

	public float Mana = 0.0f;
	public float ManaRegen = 0.2f;
	public float StandingRegenMultiplier = 2.0f;
	public float MaxMana = 20.0f;

	public LayerMask GroundRayLayerMask;

	public ParticleSystem ColdFeetParticles;
	public ParticleSystem BreezeParticles;
	public Renderer ShieldRenderer;

	private Rigidbody rigidBody;
	private bool onGround = false;
	private Ray groundRay;

	private int jumpAvailable = 0;

	public int playerIndex;
	public InputManager.ControllerState.CONTROLLERTYPE controllerType;
	private InputManager.ControllerState previousState;

	public PlayerBehaviour otherPlayer;
	private bool jumpReleased = true;

	public Image manaCircleImage;

	public Animation animator;

	void Start()
	{
		rigidBody = GetComponent<Rigidbody>();

		if (manaCircleImage != null)
			manaCircleImage.fillAmount = 0f;
	}

	void Update()
	{
		onGround = false;


		InputManager.ControllerState controllerState = InputManager.Instance.getControllerState(playerIndex, controllerType);

		Move(controllerState.leftStick.x * reverseControls);

		if (controllerState.buttonStates.A == XInputDotNetPure.ButtonState.Pressed && jumpReleased)
		{
			Jump();
		}

		if (transform.position.y < -5.0f)
			Death(false);

		rigidBody.velocity = new Vector3(rigidBody.velocity.x * HorizontalFriction, rigidBody.velocity.y, rigidBody.velocity.z);

		if (rigidBody.velocity.magnitude < 0.5f)
		{
			Mana += ManaRegen * Time.deltaTime * StandingRegenMultiplier;
		}
		else
		{
			Mana += ManaRegen * Time.deltaTime;
		}
		Mana = Mathf.Clamp(Mana, 0.0f, MaxMana);

		if (manaCircleImage != null)
			manaCircleImage.fillAmount = Mana / MaxMana;

		previousState = InputManager.Instance.getControllerState(playerIndex, controllerType);
		if (previousState.buttonStates.A == XInputDotNetPure.ButtonState.Released)
			jumpReleased = true;


		if (GetComponent<RitualBehaviour>().ColdFeetActive)
		{
			ColdFeetParticles.Emit(1);
		}
		if (GetComponent<RitualBehaviour>().BreezeActive)
		{
			BreezeParticles.Emit(1);
		}

		ShieldRenderer.enabled = GetComponent<RitualBehaviour>().ShieldActive;
		if (GetComponent<RitualBehaviour>().ShieldActive)
		{
			ShieldRenderer.transform.Rotate(Mathf.Sin(Time.time), Mathf.Sin(Time.time * 0.5f), Mathf.Cos(Time.time));
			ShieldRenderer.transform.localScale = new Vector3(
				Mathf.Abs(Mathf.Sin(Time.time)) * 0.5f + 2.5f,
				Mathf.Abs(Mathf.Sin(Time.time * 0.5f)) * 0.5f + 2.5f,
				Mathf.Abs(Mathf.Cos(Time.time)) * 0.5f + 2.5f);

			ShieldRenderer.material.color = new Color(0.6f, 0.6f, 1.0f, Mathf.Sin(Time.time) * 0.5f + 0.5f);
		}

		if (animator != null)
		{
			animator["Walk"].speed = Mathf.Clamp(rigidBody.velocity.x * 5.0f, 0.0f, 5.0f);
			if (!animator.isPlaying)
			{
				animator.Play("Walk");
			}
		}

		if (Mathf.Abs(rigidBody.velocity.y) > 2.0f || jumpAvailable != MaxJumps)
		{
			if (animator)
				animator.Play("Jump");
		}
	}

	void OnCollisionEnter(Collision collisionInfo)
	{
		if (collisionInfo.gameObject.layer == 8)
		{
			if (transform.position.y > (collisionInfo.gameObject.transform.position.y - 0.2f))
			{
				jumpAvailable = MaxJumps;
			}
		}
	}

	public void Death(bool byDemon)
	{
		if (byDemon)
			PlaySound("demon");
		else
			PlaySound("fall");

		PlayerSpawner.ReloadLevel();

		GameObject.Destroy(this.gameObject);
	}

	public void Move(float xAxis)
	{
		if (Mathf.Abs(rigidBody.velocity.x) < MaxSpeed)
			rigidBody.AddForce(new Vector3(xAxis * 10.0f * SpeedMult * RitualAllocPenalty, 0, 0));
	}


	public void PlaySound(string name)
	{
		List<AudioClip> matches = new List<AudioClip>();
		foreach (AudioClip clip in ClipList)
		{
			if (clip.name.Contains(name))
			{
				matches.Add(clip);
			}
		}

		if (matches.Count == 0)
			return;


		int randomId = Random.Range(0, matches.Count);
		Camera.main.gameObject.GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
		Camera.main.gameObject.GetComponent<AudioSource>().PlayOneShot(matches[randomId]);
	}

	public void Jump()
	{
		jumpReleased = false;
		if (jumpAvailable > 0 && (rigidBody.velocity.y < 0.1f || Mathf.Abs(rigidBody.velocity.y) < 1.0f))
		{
			PlaySound("Jump");
			animator.Play("Jump");

			jumpAvailable--;
			if (rigidBody.velocity.y < 0)
			{
				rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0.0f, rigidBody.velocity.z);
			}
			rigidBody.AddForce(new Vector3(0, JumpForce, 0));
		}
	}
}
