﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldGenerator : MonoBehaviour
{
	public GameObject StartBlock;
	public List<GameObject> Blocks = new List<GameObject>();
	public List<GameObject> PitFalls = new List<GameObject>();


	public int safeLevel = 2;

	public Vector3 LastObjectPos;
	private bool lastPitfall = true;

	// Use this for initialization
	void Start()
	{
		LastObjectPos = StartBlock.transform.position + (GetOffset(StartBlock) * 0.5f);
	}

	// Update is called once per frame
	void Update()
	{
		//Get Furthest Player
		if (Camera.main.GetComponent<GameCamera>().Target != null)
		{
			float playerX = Camera.main.GetComponent<GameCamera>().Target.position.x;

			if (Mathf.Abs(LastObjectPos.x - playerX) < 50.0f)
			{
				if (Random.Range(0, safeLevel) != 0 || lastPitfall == true)
				{
					int randomBlock = Random.Range(0, Blocks.Count);
					SpawnBlock(Blocks[randomBlock]);
					lastPitfall = false;
				}
				else
				{
					int randomBlock = Random.Range(0, PitFalls.Count);
					SpawnBlock(PitFalls[randomBlock]);
					lastPitfall = true;
				}
			}
		}
	}

	Vector3 GetOffset(GameObject gameObject)
	{
		Mesh mesh = gameObject.GetComponent<MeshFilter>().sharedMesh;
		Bounds bounds = mesh.bounds;
		return new Vector3(bounds.size.x * gameObject.transform.localScale.x, 0.0f, 0.0f);
	}

	Vector3 GetMeshSize(GameObject gameObject)
	{
		Mesh mesh = gameObject.GetComponent<MeshFilter>().sharedMesh;
		Bounds bounds = mesh.bounds;
		return bounds.size;
	}

	void SpawnBlock(GameObject block)
	{
		Vector3 newPos = LastObjectPos + (GetOffset(block) * 0.5f);

		GameObject newBlock = GameObject.Instantiate<GameObject>(block);
		newBlock.transform.position = newPos;

		LastObjectPos = newPos + (GetOffset(block) * 0.5f);
	}
}
