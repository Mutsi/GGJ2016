﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour
{
	public Transform Target;
	public float KillRange = 1.0f;
	public float DemonSpeed = 0.1f;
	public Vector3 Offset = Vector3.zero;
	private float demonX = 0.0f;

	PlayerBehaviour[] players;

	// Use this for initialization
	void Start()
	{
		demonX = transform.position.x;


	}

	void CollidePlanesWithPlayers(PlayerBehaviour[] players)
	{
		players = GameObject.FindObjectsOfType<PlayerBehaviour>();

		foreach (Plane plane in GeometryUtility.CalculateFrustumPlanes(Camera.main))
		{
			if (Vector3.Dot(Vector3.right, plane.normal) > 0.5f)
				foreach (PlayerBehaviour player in players)
				{
					if (plane.GetDistanceToPoint(player.gameObject.transform.position) < KillRange)
					{
						player.Death(true);
					}

				}
		}

	}

	// Update is called once per frame
	void FixedUpdate()
	{
		bool targetFound = false;
		Target = transform;
		float distanceToPlayer = float.MinValue;

		CollidePlanesWithPlayers(players);

		players = GameObject.FindObjectsOfType<PlayerBehaviour>();

		foreach (PlayerBehaviour player in players)
		{
			if (player.transform.position.x > distanceToPlayer)
			{
				Target = player.transform;
				targetFound = true;
				distanceToPlayer = player.transform.position.x;
			}
		}

		if (targetFound)
		{
			Vector3 targetPostion = new Vector3(Target.position.x, Target.position.y, transform.position.z);
			targetPostion.x = Mathf.Max(targetPostion.x, demonX);

			targetPostion += Offset;
			transform.position = Vector3.Lerp(transform.position, targetPostion, Time.deltaTime);
		}


		demonX += DemonSpeed;
	}
}
