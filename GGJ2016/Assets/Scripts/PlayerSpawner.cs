﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class PlayerSpawner : MonoBehaviour {

	public List<GameObject> playerList = new List<GameObject>();

	void Awake () {
        int player1Wizard = PlayerPrefs.GetInt("wizardIndexPlayer1", 0);
		int player2Wizard = PlayerPrefs.GetInt("wizardIndexPlayer2", 1);

		GameObject player1Object = (GameObject)Instantiate(playerList[player1Wizard], this.transform.position + Vector3.back * 2f, Quaternion.identity);
		GameObject player2Object = (GameObject)Instantiate(playerList[player2Wizard], this.transform.position + Vector3.forward * 2f, Quaternion.identity);

		player1Object.GetComponent<PlayerBehaviour>().playerIndex = 0;
		player2Object.GetComponent<PlayerBehaviour>().playerIndex = 1;

		player1Object.GetComponent<PlayerBehaviour>().otherPlayer = player2Object.GetComponent<PlayerBehaviour>();
		player2Object.GetComponent<PlayerBehaviour>().otherPlayer = player1Object.GetComponent<PlayerBehaviour>();

	}
	
	public static void ReloadLevel()
	{
		SceneManager.LoadScene(1);
	}

	void Update () {
	
	}
}
